import Lists from '../../api/lists/lists-collection';
import { ContactSchema } from '../../api/contacts/contacts-collection';

Template.registerHelper('displayCampaignDate', function (campaign) {
  const field = campaign.dateRef;

  const fieldRef = ContactSchema._schema[field].label || field;
  if (!campaign.delay) {
    return `le jour de ${fieldRef}`;
  }
  const { number, period, position } = campaign.delay;

  let jour = '';
  if (period === 'day') {
    _period = 'jour';
    if (number > 2) {
      _period = 'jours';
    }
  } else if (period === 'week') {
    _period = 'semaine';
    if (number > 2) {
      _period = 'semaines';
    }
  } else if (period === 'month') {
    _period = 'mois';
  } else {
    _period = 'an';
    if (number > 2) {
      _period = 'ans';
    }
  }
  if (position === 'before') {
    _position = 'avant';
  } else {
    _position = 'aprés';
  }
  return `${Math.abs(number)} ${_period} ${_position} la ${fieldRef}`;
});

Template.registerHelper('listName', function (listId) {
  const list = Lists.findOne(listId);
  if (!list) {
    return;
  }
  return list.name;
});

Template.registerHelper('getCount', function (name) {
  if (name) return Counter.get(name);
});
