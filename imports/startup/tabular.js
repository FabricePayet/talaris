import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import Contacts from '../api/contacts/contacts-collection';
import Lists from '../api/lists/lists-collection';
import Shops from '../api/shops/shops-collection';

if (Meteor.isClient) {
  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      decimal: '',
      emptyTable: 'Pas de résultat dans cette table',
      info: 'De _START_ à _END_ des _TOTAL_ contacts',
      infoEmpty: 'Affiche 0 à 0 des 0 contacts',
      infoFiltered: '(filtered from _MAX_ total entries)',
      infoPostFix: '',
      thousands: ',',
      lengthMenu: 'Affiche _MENU_ contacts',
      loadingRecords: 'chargement...',
      processing: 'Chargement...',
      search: 'Rechercher:',
      zeroRecords: 'Pas de résultat',
      paginate: {
        first: 'Premier',
        last: 'Dernier',
        next: 'Suivant',
        previous: 'Précédent',
      },
      aria: {
        sortAscending: ': activate to sort column ascending',
        sortDescending: ': activate to sort column descending',
      },
    },
  });
}

new Tabular.Table({
  name: 'List',
  collection: Contacts,
  responsive: true,
  autoWidth: false,
  selector(listId) {
    if (!Meteor.user()) return { forbidden: 'forbidden' };
    let shop = Meteor.user().getShop();
    if (!shop) return { forbidden: 'forbidden' };
    selector = { shopId: shop._id };
    return selector;
  },
  columns: [
    { data: 'firstname', title: 'Prénom' },
    { data: 'lastname', title: 'Nom' },
    { data: 'email', title: 'Email' },
    { data: 'phone', title: 'Téléphone' },
    { data: 'zipcode', title: 'Code Postal' },
    {
      data: 'lastCheckoutDate',
      title: 'Dernier passage',
      render: function(val, type, doc) {
        if (val instanceof Date) {
          return moment(val).calendar();
        } else {
          return 'Jamais';
        }
      },
    },
  ],
});

new Tabular.Table({
  name: 'Contacts',
  collection: Contacts,
  allow(userId) {
    return userId;
  },
  selector() {
    let shop = Meteor.user().getShop();
    if (!shop) return { forbidden: 'forbidden' };
    selector = { shopId: shop._id };
    return selector;
  },
  columns: [
    { data: 'firstname', title: 'Prénom' },
    { data: 'lastname', title: 'Nom' },
    { data: 'email', title: 'Email' },
    { data: 'phone', title: 'Téléphone' },
    { data: 'zipcode', title: 'Ville' },
    {
      data: 'lastCheckoutDate',
      title: 'Dernier passage',
      render: function(val, type, doc) {
        if (val instanceof Date) {
          return moment(val)
            .locale('fr')
            .calendar();
        } else {
          return 'Jamais';
        }
      },
    },
  ],
});

new Tabular.Table({
  name: 'Users',
  collection: Meteor.users,
  pub: 'UsersAdmin',
  allow(userId) {
    return Roles.userIsInRole(userId, ['admin']);
  },
  // order: [['createdAt', 'descending']],
  columns: [
    {
      data: 'createdAt',
      title: 'CreatedAt',
      render(val, type, doc) {
        return moment(val)
          .locale('fr')
          .calendar();
      },
    },
    {
      data: 'emails[0].address',
      title: 'Email',
    },
    {
      data: 'shop',
      title: 'Boutique',
    },
    {
      data: 'getShop()',
      title: 'SMS restant',
      render(val, type, doc) {
        if (!val) return '';
        if (!val.credits) return;
        if (!val.credits.sms) return 0;
        return val.credits.sms;
      },
    },
    {
      data: 'getShop()',
      title: "Nom d'envoi (sms)",
      render(val, type, doc) {
        if (!val) return;
        if (!val.twilioConfig) return;
        return val.twilioConfig.senderId;
      },
    },
    {
      title: 'Activé',
      data: 'emails',
      render(val, type, doc) {
        return val[0].verified ? 'Oui' : 'Non';
      },
    },
    {
      tmpl: Meteor.isClient && Template.adminEditContactIcon,
    },
  ],
});
