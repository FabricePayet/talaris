import { BrowserPolicy } from 'meteor/browser-policy';

Meteor.startup(() => {
  // BrowserPolicy.framing.allowAll()
  BrowserPolicy.framing.disallow();
  BrowserPolicy.content.allowInlineScripts();
  BrowserPolicy.content.allowEval();
  BrowserPolicy.content.allowImageOrigin('gravatar.com');
  BrowserPolicy.content.allowImageOrigin('static-v.tawk.to');
  BrowserPolicy.content.allowImageOrigin('www.googletagmanager.com');
  BrowserPolicy.content.allowScriptOrigin('embed.tawk.to');
  BrowserPolicy.content.allowFontOrigin('static-v.tawk.to');
  BrowserPolicy.content.allowFontOrigin('fonts.googleapis.com');
  BrowserPolicy.content.allowFontOrigin('fonts.gstatic.com');
  BrowserPolicy.content.allowFontOrigin('static.hotjar.com');
  BrowserPolicy.content.allowFrameOrigin('va.tawk.to');
  BrowserPolicy.content.allowFrameOrigin('vars.hotjar.com');
  BrowserPolicy.content.allowStyleOrigin('cdn.jsdelivr.net');
  BrowserPolicy.content.allowStyleOrigin('fonts.googleapis.com');
  BrowserPolicy.content.allowScriptOrigin('cdn.jsdelivr.net');
  BrowserPolicy.content.allowScriptOrigin('static.hotjar.com');
  BrowserPolicy.content.allowScriptOrigin('script.hotjar.com');
  BrowserPolicy.content.allowScriptOrigin('cdn.mxpnl.com');
  BrowserPolicy.content.allowScriptOrigin('www.googletagmanager.com');
  BrowserPolicy.content.allowScriptOrigin('www.google-analytics.com');
  BrowserPolicy.content.allowImageOrigin('www.google-analytics.com');
});
