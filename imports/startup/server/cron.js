import Campaigns from '../../api/campaigns/campaigns-collection';
import Shops from '../../api/shops/shops-collection';
import StatsApi from '../../api/stats/stats-api';
import CampaignApi from '../../api/campaigns/campaigns-api';

SyncedCron.add({
  name: 'Campaigns',
  schedule: function(parser) {
    return parser.text('at 05:00');
    // UTC
    // return parser.text('at 05:00 am every day');
    // return parser.text('every 1 min');
  },
  job: function() {
    CampaignApi.launchTodayCampaign();
  },
});

SyncedCron.add({
  name: 'Recording statistics',
  schedule: function(parser) {
    return parser.text('at 00:30 am every day');
    // return parser.text('every 10 min');
  },
  job: function() {
    StatsApi.registerStats();
  },
});

SyncedCron.start();
