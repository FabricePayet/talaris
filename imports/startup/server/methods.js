import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import Contacts from '../../api/contacts/contacts-collection';
import Shops from '../../api/shops/shops-collection';

const updateShopMethod = new ValidatedMethod({
  name: 'updateShopMethod',
  validate: new SimpleSchema({
    id: {
      type: String,
    },
    shop: {
      type: new SimpleSchema({
        description: {
          type: String,
        },
        name: {
          type: String,
        },
      }),
    },
  }).validator(),
  run({ id, shop }) {
    Shops.update(id, { $set: shop });
  },
});
