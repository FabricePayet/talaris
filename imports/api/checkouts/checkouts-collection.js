import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform']);

const Checkouts = new Mongo.Collection('checkouts');

Checkouts.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Define the schema
export const CheckoutSchema = new SimpleSchema(
  {
    description: {
      type: String,
      label: 'Description',
      optional: true,
    },
    date: {
      type: Date,
      label: 'Date',
    },
    contactId: {
      type: String,
      label: 'Contact',
      autoform: {
        omit: true,
      },
    },
    shopId: {
      type: String,
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Checkouts.attachSchema(CheckoutSchema);

export default Checkouts;
