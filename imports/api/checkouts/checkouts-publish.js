import moment from 'moment';

import Checkouts from './checkouts-collection';

Meteor.publish('lastCheckouts', function(contactId) {
  if (!this.userId) {
    return;
  }
  return Checkouts.find({ contactId }, { limit: 10, sort: { date: -1 } });
});

Meteor.publish('countWeekCheckouts', function() {
  if (!this.userId) {
    return;
  }
  const shop = Meteor.user().getShop();
  const beginWeekDate = moment()
    .startOf('week')
    .toISOString();
  return new Counter(
    'nbWeekCheckouts',
    Checkouts.find({
      shopId: shop._id,
      createdAt: { $gte: new Date(beginWeekDate) },
    }),
  );
});
