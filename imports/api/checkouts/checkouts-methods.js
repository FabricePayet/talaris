import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import Contacts from '../contacts/contacts-collection';
import Checkouts from './checkouts-collection';
import Lists from '../lists/lists-collection';
import Shops from '../shops/shops-collection';

submitAddCheckout = new SimpleSchema({
  description: {
    type: String,
    optional: true,
  },
  contactId: {
    type: String,
  },
});

const addCheckout = new ValidatedMethod({
  name: 'addCheckout',
  validate: submitAddCheckout.validator(),
  run(checkout) {
    const contact = Contacts.findOne(checkout.contactId);
    const shop = Shops.findOne({ ownerId: Meteor.userId() });
    if (contact.shopId !== shop._id) {
      throw new Error('The contact should be related to the owned shop');
    }
    checkout.date = new Date();
    checkout.shopId = Meteor.user().getShop()._id;
    Checkouts.insert(checkout);
  },
});
