import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform']);

const Stats = new Mongo.Collection('statistics');

Stats.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

const StatCountSchema = new SimpleSchema({
  dayCount: Number,
  weekCount: Number,
});

// Define the schema
export const StatSchema = new SimpleSchema(
  {
    contacts: {
      type: StatCountSchema,
      optional: true,
    },
    sms: {
      type: StatCountSchema,
      optional: true,
    },
    checkouts: {
      type: StatCountSchema,
      optional: true,
    },

    shopId: {
      type: String,
      label: 'Shop',
      autoform: {
        omit: true,
      },
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Stats.attachSchema(StatSchema);

export default Stats;
