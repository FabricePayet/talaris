import moment from 'moment';

import Stats from './stats-collection';
import Shops from '../shops/shops-collection';
import Sms from '../sms/sms-collection';
import Contacts from '../contacts/contacts-collection';
import Checkouts from '../checkouts/checkouts-collection';

const StatsApi = {
  registerStats() {
    const shops = Shops.find().fetch();
    const dayLimit = moment()
      .add(-1, 'days')
      .toISOString();
    const weekLimit = moment()
      .add(-7, 'days')
      .toISOString();

    shops.forEach(function(shop) {
      // Stats for SMS
      const daySmsCount = Sms.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(dayLimit) },
      }).count();

      const weekSmsCount = Sms.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(weekLimit) },
      }).count();

      // Stats for contacts
      const dayContactsCount = Contacts.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(dayLimit) },
      }).count();

      const weekContactsCount = Contacts.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(weekLimit) },
      }).count();

      // Stats for checkouts
      const dayCheckoutsCount = Checkouts.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(dayLimit) },
      }).count();

      const weekCheckoutsCount = Checkouts.find({
        shopId: shop._id,
        createdAt: { $gt: new Date(weekLimit) },
      }).count();

      Stats.insert({
        shopId: shop._id,
        checkouts: {
          dayCount: dayCheckoutsCount,
          weekCount: weekCheckoutsCount,
        },
        contacts: {
          dayCount: dayContactsCount,
          weekCount: weekContactsCount,
        },
        sms: {
          dayCount: daySmsCount,
          weekCount: weekSmsCount,
        },
      });
    });
  },
};

export default StatsApi;
