import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform']);

const Sms = new Mongo.Collection('sms');

Sms.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export const SmsSchema = new SimpleSchema(
  {
    message: {
      type: String,
      label: 'Message',
    },
    contactId: {
      type: String,
      label: 'Contact',
    },
    campaignId: {
      type: String,
      label: 'Campagne',
    },
    shopId: {
      type: String,
      label: 'Magasin',
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Sms.attachSchema(SmsSchema);

export default Sms;
