import Sms from './sms-collection';
import Shops from '../shops/shops-collection';
import TwilioApi from '../twilio/twilio-api';

const smsApi = {
  sendSms(message, contactId, campaignId, shopId) {
    const shop = Shops.findOne(shopId);
    if (shop.state === 'out' || shop.state === 'expired') {
      console.log('Impossible to send sms shop state', shop.state);
      return;
    }
    TwilioApi.sendMessage(message, shop, contactId);
    Sms.insert(
      {
        message,
        contactId,
        campaignId,
        shopId,
      },
      err => {
        if (err) {
          return console.error(
            'An error happened when sending SMS:',
            err.message,
          );
        }
        Shops.update(shopId, { $inc: { 'credits.sms': -1 } });
        if (shop.credits.sms - 1 === 0) {
          Shops.update(shopId, { $set: { state: 'out' } });
        }
      },
    );
  },
};

export default smsApi;
