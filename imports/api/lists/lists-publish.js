import Lists from './lists-collection';
import Shops from '../shops/shops-collection';

Meteor.publish('lists', function() {
  if (!this.userId) {
    return;
  }
  const shop = Shops.findOne({ ownerId: this.userId });
  if (!shop) return;
  return Lists.find({ shopId: shop._id });
});

Meteor.publish('listDetail', function(listId) {
  if (!this.userId) {
    return;
  }
  const shop = Shops.findOne({ ownerId: this.userId });
  return Lists.find({ shopId: shop._id, _id: listId });
});
