import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import EmailApi from '../email/email-api';

new ValidatedMethod({
  name: 'register.new',
  validate: new SimpleSchema({
    email: { type: String, regEx: SimpleSchema.RegEx.Email },
    firstname: { type: String },
    lastname: { type: String },
    phone: {type: String, regEx: SimpleSchema.RegEx.Phone},
    company: {type: String}
  }).validator(),
  run({ email, firstname, lastname, phone, company }) {
    message = `Nouvelle inscription sur Talaris:\n\n${firstname} ${lastname}\n${company}\n${email}\n${phone}`
    EmailApi.sendEmail('contact@talaris.io', 'Nouvelle inscription', message)
  },
});
