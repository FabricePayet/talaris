import moment from 'moment';
import removeAccents from 'remove-accents';

import Shops from './shops-collection';
import Campaigns from '../campaigns/campaigns-collection';
import TwilioApi from '../twilio/twilio-api';
import EmailApi from '../email/email-api';

import { Email } from 'meteor/email';

const shopApi = {
  createNewShop(name, ownerId) {
    const newShop = {
      name,
      ownerId,
    };
    TwilioApi.createSubAccount(
      name,
      Meteor.bindEnvironment(function(err, account) {
        if (err) {
          console.error('Error when adding a subaccount:', err);
          return;
        }
        const { friendlyName, sid, authToken } = account;
        console.info(`New twilio subaccount created: ${friendlyName}`);
        let senderId = removeAccents(name).replace(/[^0-9a-z ]/gi, '');
        if (senderId.length > 11) {
          senderId = senderId.substring(0, 10);
        }
        newShop.twilioConfig = {
          sid,
          authToken,
          senderId,
        };

        newShop.currentPack = {
          name: 'Beta',
          expirationDate: moment()
            .add(1, 'months')
            .toISOString(),
          sms: 10,
        };
        newShop.credits = {
          sms: 10,
        };
        let shopId = Shops.insert(newShop);
        EmailApi.sendEmail(
          'contact@talaris.io',
          'Nouveau compté créé',
          `Un nouveau compte a été créé sur Talaris : ${name}`,
        );
      }),
    );
  },
};

export default shopApi;
