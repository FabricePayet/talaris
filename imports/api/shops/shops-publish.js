import Shops from './shops-collection';

Meteor.publish('usershop', function() {
  if (!this.userId) {
    return;
  }
  return Shops.find(
    { ownerId: this.userId },
    {
      fields: {
        name: 1,
        description: 1,
        ownerId: 1,
        currentPack: 1,
        credits: 1,
        createdAt: 1,
      },
    },
  );
});

Meteor.publish('shopDetailAdmin', function(ownerId) {
  if (!this.userId) {
    return;
  }

  if (!Roles.userIsInRole(this.userId, ['admin'])) {
    console.error('not admin');
    this.stop();
    return;
  }

  return Shops.find({ ownerId });
});
