import { check, Match } from 'meteor/check';
import Shops from '../../api/shops/shops-collection';

Meteor.publishComposite('UsersAdmin', function(tableName, ids, fields) {
  check(tableName, String);
  check(ids, Array);
  check(fields, Match.Optional(Object));

  // this.unblock(); // requires meteorhacks:unblock package

  return {
    find: function() {
      // this.unblock(); // requires meteorhacks:unblock package

      // check for admin role with alanning:roles package
      if (!Roles.userIsInRole(this.userId, 'admin')) {
        return this.stop();
      }

      return Meteor.users.find({ _id: { $in: ids } }, { fields });
    },
    children: [
      {
        find: function(user) {
          // this.unblock(); // requires meteorhacks:unblock package
          // Publish the related user
          return Shops.find({ ownerId: user._id });
        },
      },
    ],
  };
});
