import { Email } from 'meteor/email';

export default (emailApi = {
  sendEmail(to, subject, message) {
    const from = 'noreply@talaris.io';
    let env = '';
    if (!Meteor.isProduction) {
      env = '[DEV] ';
    }
    const text = `${env}${message}`;
    Email.send({ to, from, subject, text });
  },

  sendCampaignFailed(to, campaignName) {
    const from = 'noreply@talaris.io';
    const subject = 'Impossible de lancer votre campagne';
    const text = `L'envoi de SMS pour la campagne "${campaignName}" a échoué, pensez à ajouter du crédit sur Talaris ou désactiver la campagne en cours.\n\n En cas de problème, contacter notre support contact@talaris.io`;
    Email.send({ to, from, subject, text });
  },
});
