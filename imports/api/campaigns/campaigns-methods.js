import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import Shops from '../shops/shops-collection';
import Campaigns, { CampaignSchema } from './campaigns-collection';

const addCampaign = new ValidatedMethod({
  name: 'addCampaign',
  validate: CampaignSchema.validator(),
  run(campaign) {
    if (!this.userId) {
      throw new Meteor.Error(
        'Campaigns.methods.addCampaign.notLoggedIn',
        'Must be logged to activate Campaigns.',
      );
    }
    const shop = Shops.findOne({ ownerId: this.userId });
    campaign.shopId = shop._id;
    campaign = CampaignSchema.clean(campaign);
    Campaigns.insert(campaign);
  },
});

CampaignSchemaEdit = new SimpleSchema({
  message: String,
  shopId: String,
  createdAt: Date,
  updatedAt: Date,
});

const editCampaign = new ValidatedMethod({
  name: 'editCampaign',
  validate: new SimpleSchema({
    _id: String,
    modifier: new SimpleSchema({
      $set: CampaignSchemaEdit,
    }),
  }).validator(),
  run({ _id, modifier }) {
    if (!this.userId) {
      throw new Meteor.Error(
        'Campaigns.methods.editCampaign.notLoggedIn',
        'Must be logged to edit Campaigns.',
      );
    }
    const shop = Meteor.user().getShop();
    if (modifier.$set.shopId !== shop._id) {
      throw new Meteor.Error(
        'Campaigns.methods.editCampaign.shopNotOwned',
        'Must be the shop owner.',
      );
    }
    Campaigns.update(_id, modifier);
  },
});

const deleteCampaign = new ValidatedMethod({
  name: 'deleteCampaign',
  validate: new SimpleSchema({
    campaignId: { type: String },
  }).validator(),
  run({ campaignId }) {
    if (!this.userId) {
      throw new Meteor.Error(
        'Campaigns.methods.deleteCampaign.notLoggedIn',
        'Must be logged to activate Campaigns.',
      );
    }
    const shop = Shops.findOne({ ownerId: this.userId });
    const campaign = Campaigns.findOne(campaignId);
    if (campaign.shopId !== shop._id) {
      throw new Meteor.Error(
        'Campaigns.methods.deleteCampaign.notOwner',
        "Must be the shop's owner.",
      );
    }
    Campaigns.remove(campaignId);
  },
});
