import { publishComposite } from 'meteor/reywood:publish-composite';

import Campaigns from './campaigns-collection';
import Shops from '../shops/shops-collection';
import Lists from '../lists/lists-collection';

publishComposite('campaigns', {
  find() {
    if (!this.userId) {
      return;
    }
    const shop = Shops.findOne({ ownerId: this.userId });
    if (!shop) return;
    return Campaigns.find({ shopId: shop._id });
  },
  children: [
    {
      find(campaign) {
        return Lists.find({ _id: { $in: campaign.lists } });
      },
    },
  ],
});

Meteor.publish('campaignDetail', function(campaignId) {
  if (!this.userId) {
    return;
  }
  const shop = Shops.findOne({ ownerId: this.userId });
  if (!shop) return;
  return Campaigns.find({ shopId: shop._id, _id: campaignId });
});
