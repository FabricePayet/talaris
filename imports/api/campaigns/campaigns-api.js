import moment from 'moment';

import Shops from '../shops/shops-collection';
import Campaigns from './campaigns-collection';
import SmsApi from '../../api/sms/sms-api';
import MailApi from '../../api/email/email-api';
import Contacts from '../../api/contacts/contacts-collection';

getContactsFromCampaignLists = function(campaign, shopId) {
  let contacts = [];
  if (!campaign.lists || campaign.lists.length === 0) {
    contacts = Contacts.find({ shopId }).fetch();
  } else {
    let contactsList = [];
    campaign.lists.forEach(function(listId) {
      contacts = Contacts.find({ shopId, lists: listId }).fetch();
      contactsList = contactsList.concat(contacts);
    });
    contacts = _.uniq(contactsList);
  }
  return contacts;
};

checkDateForContacts = function(campaign, contacts) {
  const finalContacts = [];
  contacts.forEach(function(contact) {
    if (contact[campaign.dateRef]) {
      let executionDate;
      if (campaign.delay) {
        const { period, number, position } = campaign.delay;
        let nb = number;
        if (position === 'before') {
          nb = number * -1;
        }
        executionDate = moment(contact[campaign.dateRef])
          .add(nb, period)
          .startOf('day');
      } else {
        executionDate = moment(contact[campaign.dateRef]).startOf('day');
      }

      if (campaign.frequency) {
        if (campaign.frequency.unit === 'year') {
          const currentYear = moment().format('YYYY');
          const dateExecutionDayAndMonth = executionDate.format('DD-MM');
          executionDate = moment(
            `${dateExecutionDayAndMonth}-${currentYear}`,
            'DD-MM-YYYY',
          );
        }
      }
      const startDay = moment().startOf('day');

      // to remove
      console.info(
        'automatic campaign for contact',
        contact.firstname,
        contact.lastname,
        startDay.diff(executionDate, 'days'),
      );

      if (startDay.diff(executionDate, 'days') === 0) {
        finalContacts.push(contact);
      }
    }
  });
  return finalContacts;
};

// replace template item
const _formatMessage = function(message, shop, contact) {
  message = message.replace('{shop}', shop.name);
  return message;
};

const _hasEnoughCredits = function(contacts, shop) {
  if (!shop.credits) return false;
  return shop.credits.sms >= contacts.length;
};

const CampaignApi = {
  launchTodayCampaign() {
    const shops = Shops.find().fetch();
    shops.forEach(function(shop) {
      console.info('[CAMPAIGN] analysing shop ', shop.name);
      let campaigns = Campaigns.find({
        shopId: shop._id,
        state: 'active',
      }).fetch();
      if (!campaigns.length) {
        return;
      }
      if (shop.state === 'ok' || shop.state === 'trial') {
        campaigns.forEach(function(campaign) {
          console.info('[CAMPAIGN] beginning campaign ', campaign.name);
          // CAMPAIGNS WITH DATE FIXED
          if (campaign.type === 'planned') {
            let executionDateMoment = moment(campaign.executionDate).startOf(
              'day',
            );
            const today = moment().startOf('day');
            if (campaign.frequency) {
              if (campaign.frequency.unit === 'year') {
                const currentYear = moment().format('YYYY');
                const dateExecutionDayAndMonth = executionDateMoment.format(
                  'DD-MM',
                );
                executionDateMoment = moment(
                  `${dateExecutionDayAndMonth}-${currentYear}`,
                  'DD-MM-YYYY',
                );
              }
            }
            console.info(
              'planned diff:',
              today.diff(executionDateMoment, 'days'),
            );
            if (today.diff(executionDateMoment, 'days') === 0) {
              console.info('Campaign planned for today:', campaign.name);
              let contacts = getContactsFromCampaignLists(campaign, shop._id);
              if (_hasEnoughCredits(contacts, shop)) {
                contacts.forEach(function(contact) {
                  message = _formatMessage(campaign.message, shop, contact);
                  SmsApi.sendSms(message, contact._id, campaign._id, shop._id);
                });
                Campaigns.update(campaign._id, {
                  $set: {
                    'sentCount.sms': contacts.length,
                    state: 'completed',
                  },
                });
              } else {
                console.warn(
                  'Not enough credits to launch this campaign: ',
                  campaign._id,
                );
                MailApi.sendCampaignFailed(shop.getEmail(), campaign.name);
              }
            }
          } else {
            // CAMPAIGN AUTOMATIC
            let contacts = getContactsFromCampaignLists(campaign, shop._id);
            const finalContacts = checkDateForContacts(campaign, contacts);
            if (_hasEnoughCredits(finalContacts, shop)) {
              finalContacts.forEach(function(contact) {
                message = _formatMessage(campaign.message, shop, contact);
                SmsApi.sendSms(message, contact._id, campaign._id, shop._id);
              });
              Campaigns.update(campaign._id, {
                $inc: { 'sentCount.sms': finalContacts.length },
              });
            } else {
              console.warn(
                'Not enough credits to launch this campaign: ',
                campaign._id,
              );
              MailApi.sendCampaignFailed(shop.getEmail(), campaign.name);
            }
          }
        });
      } else {
        switch (shop.state) {
          case 'expired':
            console.warn(
              `[${
                shop._id
              }] Impossible d\'envoyer un sms. Le forfait est expiré`,
            );
            break;
          case 'trial':
            console.warn(
              `[${
                shop._id
              }] Impossible d\'envoyer un sms. Trial version cannot sent sms`,
            );
            break;
          case 'out':
            console.warn(
              `[${
                shop._id
              }] Impossible d\'envoyer un sms. Le forfait est dépassé`,
            );
            break;
          default:
            console.warn(
              `[${shop._id}] Impossible d\'envoyer un sms: ${shop.state}`,
            );
        }
      }
    });
  },
};

export default CampaignApi;
