import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Shops from '../shops/shops-collection';

SimpleSchema.extendOptions(['autoform']);

const Contacts = new Mongo.Collection('contacts');

Contacts.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Define the schema
export const ContactSchema = new SimpleSchema(
  {
    firstname: {
      type: String,
      label: 'Prénom',
    },
    lastname: {
      type: String,
      label: 'Nom',
      optional: true,
    },
    email: {
      type: String,
      label: 'Adresse email',
      regEx: SimpleSchema.RegEx.Email,
      optional: true,
    },
    phone: {
      type: String,
      label: 'Numéro de téléphone',
      regEx: /^[\+0](262)?[0-9]{9}$/,
    },
    zipcode: {
      type: String,
      label: 'Code Postal',
      optional: true,
      regEx: SimpleSchema.RegEx.ZipCode,
    },
    lastCheckoutDate: {
      type: Date,
      optional: true,
      label: 'Date du dernier passage',
    },
    birthdayDate: {
      type: Date,
      optional: true,
      label: "Date d'anniversaire",
    },
    gender: {
      type: String,
      optional: true,
      allowedValues: ['male', 'female'],
    },
    shopId: {
      type: String,
      label: 'Shop',
      autoValue() {
        return Shops.findOne({ ownerId: this.userId })._id;
      },
      autoform: {
        omit: true,
      },
    },
    channel: {
      type: String,
      label: 'Channel',
      optional: true,
      autoform: {
        omit: true,
      },
    },
    sexe: {
      type: String,
      allowedValues: ['man', 'woman'],
      optional: true,
    },
    lists: {
      type: Array,
      optional: true,
    },
    'lists.$': {
      type: String,
    },
    note: {
      type: String,
      optional: true,
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Contacts.attachSchema(ContactSchema);

export default Contacts;
