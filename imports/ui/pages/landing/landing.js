import '../../../../node_modules/aos/dist/aos.css';
import AOS from 'aos';

import './landing.html';

Template.Landing_Page.onCreated(function () {
  // $(window).on('load', function () {
  //   $('#page-loader').fadeOut(500);
  // });
  this.errorMessage = new ReactiveVar(null);
  this.registerDone = new ReactiveVar(false);
  this.isLoading = new ReactiveVar(false);
});

Template.Landing_Page.onRendered(function () {
  AOS.init();

  mixpanel.track('landing_loaded');

  function fixedHeader() {
    const ww = $(window).scrollTop();
    if (ww > 0) {
      $('.navbar').addClass('navbar--active');
      $('.mobile-menu').addClass('mobile-navbar--active');
    } else {
      $('.navbar').removeClass('navbar--active');
      $('.mobile-menu').removeClass('mobile-navbar--active');
    }
  }
  fixedHeader();
  $(window).on('scroll', function () {
    fixedHeader();
  });
});


Template.Landing_Page.events({
  'submit #registerForm__form'(event, instance) {
    event.preventDefault();

    $registerEmail = instance.$('#registerEmail');
    $registerPhone = instance.$('#registerPhone');
    $registerCompany = instance.$('#registerCompany');
    $registerFirstname = instance.$('#registerFirstname');
    $registerLastname = instance.$('#registerLastname');

    if (!$registerEmail.val() || !$registerPhone.val() || !$registerCompany.val() || !$registerFirstname.val() || !$registerLastname.val()) {
      instance.errorMessage.set('Veuillez remplir tous les champs');
    }

    instance.isLoading.set(true);

    Meteor.call('register.new', {
      email: $registerEmail.val(),
      phone: $registerPhone.val(),
      company: $registerCompany.val(),
      firstname: $registerFirstname.val(),
      lastname: $registerLastname.val(),
    }, (err) => {
      if (err) {
        instance.errorMessage.set(err.reason);
        return;
      }

      mixpanel.track('register_done');

      instance.registerDone.set(true);
    });
  },

  'shown.bs.modal #registerForm'(event, instance) {
    mixpanel.track('register_modal_opened');
  },
});

Template.Landing_Page.helpers({
  errorMessage() {
    return Template.instance().errorMessage.get();
  },

  registerDone() {
    return Template.instance().registerDone.get();
  },

  isLoading() {
    return Template.instance().isLoading.get();
  },
});
