import './reset-password.html';

Template.Reset_Password_Page.events({
  'submit form'(event, instance) {
    event.preventDefault();
    Accounts.resetPassword(
      FlowRouter.getParam('token'),
      instance.$('input').val(),
      err => {
        if (err) {
          toastr.error(err.message);
        } else {
          toastr.success('Votre mot de passe a été changé');
          FlowRouter.go('/');
        }
      },
    );
  },
});
