import './list-detail.html';

import Lists from '../../../api/lists/lists-collection';
import Contacts from '../../../api/contacts/contacts-collection';

Template.List_Detail_Page.onCreated(function() {
  let handle = this.subscribe('listDetail', FlowRouter.getParam('listId'));
  this.isReady = new ReactiveVar(false);
  this.autorun(() => {
    this.isReady.set(handle.ready());
  });
});

Template.List_Detail_Page.helpers({
  isReady() {
    return Template.instance().isReady.get();
  },

  list() {
    return Lists.findOne(FlowRouter.getParam('listId'));
  },

  selector() {
    const selector = { lists: FlowRouter.getParam('listId') };
    return selector;
  },

  breadcrumdData() {
    const list = Lists.findOne(FlowRouter.getParam('listId'));
    if (!list) return;
    return {
      parents: [{ routeName: 'lists', friendlyName: 'Listes' }],
      current: {
        friendlyName: list.name,
      },
    };
  },

  contactsCount() {
    const listId = FlowRouter.getParam('listId');
    const list = Lists.findOne(listId);
    if (!list) return;
    return list.count;
  },
});

Template.List_Detail_Page.events({
  'click .js-delete-list'(event, instance) {
    bootbox.confirm({
      message: 'Êtes-vous sûr de vouloir supprimer cette liste ?',
      buttons: {
        confirm: {
          label: 'Supprimer',
          className: 'btn-danger',
        },
        cancel: {
          label: 'Annuler',
          className: 'btn-light',
        },
      },
      callback: result => {
        if (result) {
          Meteor.call(
            'deleteListMethod',
            { listId: FlowRouter.getParam('listId') },
            err => {
              if (err) {
                toastr.error(err.message);
              } else {
                toastr.success('La liste a été supprimée');
                FlowRouter.go('contacts');
              }
            },
          );
        }
      },
    });
  },

  'click tbody > tr'(event) {
    var dataTable = $(event.target)
      .closest('table')
      .DataTable();
    var rowData = dataTable.row(event.currentTarget).data();
    if (!rowData) return; // Won't be data if a placeholder row is clicked

    FlowRouter.go('contactDetail', { contactId: rowData._id });
  },
});
