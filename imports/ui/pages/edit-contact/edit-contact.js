import './edit-contact.html';

import { ContactSchema } from '../../../api/contacts/contacts-collection';

const hooksObject = {
  onError: function(formType, error) {
    console.error(error);
  },

  before: {
    method: function(doc) {
      doc._id = FlowRouter.getParam('contactId');
      if (doc.phone) {
        if (doc.phone.startsWith('069')) {
          doc.phone = '+262' + doc.phone.substring(1);
        }
      }
      return doc;
    },
  },
  onSuccess: function() {
    toastr.success('Le contact à bien été modifié');
    $('.edit-contact-modal').modal('toggle');
  },

  beginSubmit: function() {
    $('.edit-contact-modal input[type="submit"]').attr('disabled', 'disabled');
  },
  endSubmit: function() {
    $('.edit-contact-modal input[type="submit"]').removeAttr('disabled');
  },
};

AutoForm.addHooks('editContact', hooksObject, true);


Template.Edit_Contact_Modal.helpers({
  ContactSchema() {
    return ContactSchema.extend({
      _id: {
        type: String,
      },
    }).omit('shopId');
  },
});
