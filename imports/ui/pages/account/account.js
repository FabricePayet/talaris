import './account.html';

Template.Account_Page.helpers({
  shop() {
    if (!Meteor.user()) {
      return;
    }
    return Meteor.user().getShop();
  },

  adminEmail() {
    return Meteor.user().emails[0].address;
  },
});

/* *************************** Change_Password ******************************** */

Template.Change_Password.onCreated(function() {
  this.errorMessage = new ReactiveVar(null);
});

Template.Change_Password.events({
  'click .js-change-password'(event, instance) {
    event.preventDefault();

    $oldPassword = instance.$('input[name="oldPassword"]');
    $newPassword = instance.$('input[name="newPassword"]');
    $newPasswordConfirm = $('input[name="newPasswordConfirm"]');

    const oldPassword = $oldPassword.val();
    const newPassword = $newPassword.val();
    const newPasswordConfirm = $newPasswordConfirm.val();

    $newPassword.val('');
    $oldPassword.val('');
    $newPasswordConfirm.val('');

    if (!newPassword || !oldPassword || !newPasswordConfirm) {
      return;
    }

    if (newPassword !== newPasswordConfirm) {
      instance.errorMessage.set('Les mots de passe ne correspondent pas');
      return;
    }

    Accounts.changePassword(oldPassword, newPassword, err => {
      if (err) {
        if (err.error === 403) {
          return instance.errorMessage.set('Mot de passe incorrect');
        }

        return instance.errorMessage.set(err.message);
      }

      toastr.success('Le mot de passe a bien été changé');
    });
  },
});

Template.Change_Password.helpers({
  errorMessage() {
    return Template.instance().errorMessage.get();
  },
});
