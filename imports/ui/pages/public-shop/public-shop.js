import { ContactSchema } from '../../../api/contacts/contacts-collection';
import './public-shop.html';

const hooksObject = {
  onError: function(formType, error) {
    toastr.error(error.reason, 'Add error');
  },

  onSubmit: function(insertDoc, updateDoc, currentDoc) {
    Meteor.call('publicAddContactInList', {
      shop: FlowRouter.getParam('shopSlug'),
      contact: insertDoc,
    });
    // this.done();
  },
};

AutoForm.addHooks('publicAddContact', hooksObject, true);

Template.Public_Shop_Page.onCreated(function() {
  this.isReady = new ReactiveVar(false);
  let handleSubscribe = this.subscribe(
    'publicShop',
    FlowRouter.getParam('shopSlug'),
  );
  Tracker.autorun(() => this.isReady.set(handleSubscribe.ready()));
});

Template.Public_Shop_Page.helpers({
  relatedShop() {
    return Shops.findOne({ slug: FlowRouter.getParam('shopSlug') });
  },

  ContactSchema() {
    return ContactSchema;
  },

  isReady() {
    return Template.instance().isReady.get();
  },
});
