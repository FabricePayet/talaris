import './dashboard.html';

import Chart from 'chart.js';
import moment from 'moment';

import Shop from '../../../api/shops/shops-collection';
import Stats from '../../../api/stats/stats-collection';

Template.Dashboard_Page.onCreated(function() {
  handleStats = this.subscribe('stats');

  this.isReady = new ReactiveVar(false);

  this.autorun(() => {
    this.isReady.set(handleStats.ready());
  });
});

Template.Dashboard_Page.helpers({
  isReady() {
    return Template.instance().isReady.get();
  },
  ownerName() {
    return this.owner.emails[0].address;
  },

  publicLinkUrl() {
    if (!Meteor.user() || !Meteor.user().getShop()) {
      return;
    }
    const slug = Meteor.user().getShop().slug;
    return Meteor.absoluteUrl('shop/' + slug);
  },

  shop() {
    if (!Meteor.user()) {
      return;
    }
    return Meteor.user().getShop();
  },

  breadcrumdData() {
    return {
      current: {
        friendlyName: 'Tableau de bord',
      },
    };
  },

  hasStats() {
    return Stats.find({}).count() >= 7;
  },
});

Template.Dashboard_Page.events({
  'click .js-redirect-contact'(event, instance) {
    FlowRouter.go('contacts');
  },

  'click .js-redirect-feature'(event, instance) {
    const routeName = instance.$(event.currentTarget).attr('data-route');
    FlowRouter.go(routeName);
  },

  'click .js-redirect-campaigns'(event, instance) {
    FlowRouter.go('campaigns');
  },

  'click .js-twilio-pricing'(event, instance) {
    bootbox.confirm({
      message: 'Cela peut engendrer des frais supplémentaires. Etes vous sûr ?',
      buttons: {
        confirm: {
          label: 'Envoyer',
          className: 'btn-warning',
        },
        cancel: {
          label: 'Annuler',
          className: 'btn-light',
        },
      },
      callback: result => {
        if (result) {
          Meteor.call('getTwilioPricing', err => {
            if (err) {
              return toastr.error(err.message);
            }
            toastr.success('Test OK');
          });
        }
      },
    });
  },

  'click .js-twilio-buy'(event, instance) {
    Meteor.call('buyNumberForShop', err => {
      if (err) {
        return toastr.error(err.message);
      }
      toastr.success('Buy number OK');
    });
  },

  'click .js-twilio-test-message'(event, instance) {
    bootbox.confirm({
      message: 'Un Sms va être envoyé et facturé (0.14€). Etes vous sûr ?',
      buttons: {
        confirm: {
          label: 'Envoyer',
          className: 'btn-warning',
        },
        cancel: {
          label: 'Annuler',
          className: 'btn-light',
        },
      },
      callback: result => {
        if (result) {
          Meteor.call(
            'sendSmsMethod',
            { message: 'Bonjour, ceci est le message de test' },
            err => {
              if (err) {
                return toastr.error(err.message);
              }
              toastr.success('Send test OK');
            },
          );
        }
      },
    });
  },
});
