import './edit-campaign.html';

import { Session } from 'meteor/session';

import Campaigns, {
  CampaignSchema,
} from '../../../api/campaigns/campaigns-collection';

const hooksObject = {
  onError: function(formType, error) {
    toastr.error(error.reason);
  },
  before: {
    'method-update': function(doc) {
      return doc;
    },
  },
  onSuccess: function() {
    $('.edit-campaign-modal').modal('hide');
    $('#editCampaign')[0].reset();
    toastr.success('La campagne a bien été modifié');
  },
};

AutoForm.addHooks('editCampaign', hooksObject, true);

Template.Edit_Campaign_Modal.onCreated(function() {
  this.currentMessage = new ReactiveVar('');

  this.autorun(() => {
    let campaign = Session.get('editCampaign');
    if (!campaign) {
      this.currentMessage.set('');
    } else {
      this.currentMessage.set(campaign.message);
    }
  });
});

Template.Edit_Campaign_Modal.helpers({
  campaign() {
    return Session.get('editCampaign');
  },

  CampaignSchema() {
    return CampaignSchema;
  },

  remainingChars() {
    let nbChars = Template.instance().currentMessage.get().length;
    return 160 - nbChars;
  },
});

Template.Edit_Campaign_Modal.events({
  'click .js-close-modal'(event, instance) {
    instance.$('#editCampaign')[0].reset();
    instance.$('.edit-campaign-modal').modal('hide');
  },

  'keyup textarea'(event, instance) {
    event.preventDefault();
    let message = instance.$('textarea').val();
    instance.currentMessage.set(message);
  },

  'hidden.bs.modal .edit-campaign-modal'(event, instance) {
    instance.$('#editCampaign')[0].reset();
    Session.set('editCampaign', null);
  },
});
