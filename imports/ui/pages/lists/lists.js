import './lists.html';

import Lists, { ListSchema } from '../../../api/lists/lists-collection';

Template.Lists_Page.onCreated(function() {
  this.subscribe('lists');
});

Template.Lists_Page.helpers({
  lists() {
    return Lists.find();
  },

  breadcrumdData() {
    return {
      current: {
        friendlyName: 'Listes',
      },
    };
  },

  ListSchema() {
    return ListSchema;
  },

  listCount() {
    return Lists.find().count();
  },
});
