import './enroll-account.html';

Template.Enroll_Account_Page.events({
  'submit form'(event, instance) {
    event.preventDefault();
    Accounts.resetPassword(
      FlowRouter.getParam('token'),
      instance.$('input').val(),
      err => {
        if (err) {
          toastr.error(err.message);
        } else {
          toastr.success('Bravo! Votre compte est maintenant actif.');
          FlowRouter.go('/');
        }
      },
    );
  },
});
