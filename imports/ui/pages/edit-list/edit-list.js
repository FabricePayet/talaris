import './edit-list.html';

Template.Edit_List_Modal.events({
  'click .js-edit-list'(event, instance) {
    const name = instance.$('input').val();
    const description = instance.$('textarea').val();

    Meteor.call(
      'editListMethod',
      {
        list: {
          name,
          description,
          _id: FlowRouter.getParam('listId'),
        },
      },
      function(err) {
        if (err) {
          toastr.error(err);
        } else {
          toastr.success('La liste a été modifié');
          instance.$('.modal').modal('hide');
        }
      },
    );
  },
});
