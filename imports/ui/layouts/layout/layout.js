import './layout.html';

Template.Layout.onCreated(function() {
  this.subscribe('countContacts');
  this.isReady = new ReactiveVar(false);
  let handle = this.subscribe('usershop');

  this.autorun(() => {
    this.isReady.set(handle.ready());
  });
});

Template.Layout.helpers({
  isReady() {
    return Template.instance().isReady.get();
  },
});
