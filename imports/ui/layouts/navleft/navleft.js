import './navleft.html';

import Lists from '../../../../imports/api/lists/lists-collection';

Template.Navleft.onCreated(function() {
  this.subscribe('lists');
});

Template.Navleft.events({
  'click .js-nav'(event, instance) {
    event.preventDefault();
    const routeToGo = $(event.currentTarget).attr('data-route');
    FlowRouter.go(routeToGo);
  },
});

Template.Navleft.helpers({
  lists() {
    return Lists.find();
  },

  currentPack() {
    if (!Meteor.user()) {
      return;
    }
    const shop = Meteor.user().getShop();
    if (!shop) {
      return;
    }
    if (!shop.currentPack) {
      return;
    }
    return shop.currentPack;
  },

  percentSent() {
    return Math.round(
      (this.currentPack.sms - this.credits.sms) / this.currentPack.sms * 100,
    );
  },

  currentShop() {
    if (!Meteor.user()) return;
    return Meteor.user().getShop();
  },
});
