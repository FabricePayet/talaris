import './navbar.html';

import gravatarUrl from 'gravatar-url';

Template.Navbar.helpers({
  gravatarUrl() {
    const email = Meteor.user().emails[0].address;
    return gravatarUrl(email, {
      size: 28,
      default: 'mm',
    });
  },

  shopName() {
    if (!Meteor.user().getShop()) {
      return;
    }
    return Meteor.user().getShop().name;
  },
});

Template.Navbar.events({
  'click .js-new-campaign'() {
    FlowRouter.go('campaigns');
    Meteor.setTimeout(function() {
      $('#addCampaignButton').click();
    }, 100);
  },
});
