import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
dataTablesBootstrap(window, $);

// Startup
import '../imports/startup/client';

// Collections
import Contacts, {
  ContactSchema,
} from '../imports/api/contacts/contacts-collection';
import Shops from '../imports/api/shops/shops-collection';

// Global
window.Contacts = Contacts;
window.Shops = Shops;

// Routes
import '../imports/startup/client/routes.js';

// Helpers
import '../imports/startup/client/helpers.js';

// Layout
import '../imports/ui/layouts/layout/layout.js';
import '../imports/ui/layouts/navbar/navbar.js';
import '../imports/ui/layouts/navleft/navleft.js';

// Pages
import '../imports/ui/pages/campaigns/campaigns.js';
import '../imports/ui/pages/products/products.js';
import '../imports/ui/pages/reports/reports.js';
import '../imports/ui/pages/contacts/contacts.js';
import '../imports/ui/pages/dashboard/dashboard.js';
import '../imports/ui/pages/login/login.js';
import '../imports/ui/pages/register/register.js';
import '../imports/ui/pages/send-sms/send-sms.js';
import '../imports/ui/pages/send-email/send-email.js';
import '../imports/ui/pages/update-shop/update-shop.js';
import '../imports/ui/pages/public-shop/public-shop.js';
import '../imports/ui/pages/legal/legal.js';
import '../imports/ui/pages/loading/loading.js';
import '../imports/ui/pages/configuration/configuration.js';
import '../imports/ui/pages/contact-detail/contact-detail.js';
import '../imports/ui/pages/list-detail/list-detail.js';
import '../imports/ui/pages/edit-list/edit-list.js';
import '../imports/ui/pages/edit-contact/edit-contact.js';
import '../imports/ui/pages/lists/lists.js';
import '../imports/ui/pages/invoices/invoices.js';
import '../imports/ui/pages/account/account.js';
import '../imports/ui/pages/reset-password/reset-password.js';
import '../imports/ui/pages/administration/administration.js';
import '../imports/ui/pages/enroll-account/enroll-account.js';
import '../imports/ui/pages/landing/landing.js';

// Modals
import '../imports/ui/pages/add-list/add-list.js';
// Lib
import '../imports/ui/lib/toastr';
import '../imports/ui/lib/spin';
import 'bootstrap/dist/js/bootstrap.bundle';
import '../imports/lib/moment';
// import '../imports/lib/simpl-schema';

import '../imports/ui/pages/add-contact/add-contact.js';
import '../imports/ui/pages/add-checkout/add-checkout.js';
import '../imports/ui/pages/edit-campaign/edit-campaign.js';
import '../imports/ui/pages/add-campaign/add-campaign.js';

// Component
import '../imports/ui/components/template/template.js';
import '../imports/ui/components/communication-form/communication-form.js';
import '../imports/ui/components/campaign-item/campaign-item.js';
import '../imports/ui/components/list-item/list-item.js';
import '../imports/ui/components/breadcrumb/breadcrumb.js';
import '../imports/ui/components/stat-chart/stat-chart.js';

// Other
import '../imports/startup/tabular';